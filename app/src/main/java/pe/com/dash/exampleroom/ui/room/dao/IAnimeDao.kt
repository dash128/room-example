package pe.com.dash.exampleroom.ui.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import pe.com.dash.exampleroom.ui.model.Anime

@Dao
interface IAnimeDao {
    @Insert
    fun insert(anime: Anime)

    @Update
    fun update(vararg animes: Anime)

    @Delete
    fun delete(vararg animes: Anime)

    @Query("SELECT * FROM anime WHERE id ==:id")
    fun findById(id: Int): Anime

    @Query("SELECT * FROM anime")
    fun findAll(): LiveData<List<Anime>>
}