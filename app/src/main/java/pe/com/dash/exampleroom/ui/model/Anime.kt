package pe.com.dash.exampleroom.ui.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Anime")
data class Anime (
    @PrimaryKey(autoGenerate = true)
    var id: Int?,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "imagen")
    var imagen: String,

    @ColumnInfo(name = "chapter")
    var chapter: Int
)