package pe.com.dash.exampleroom.ui.room.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import pe.com.dash.exampleroom.ui.model.Anime
import pe.com.dash.exampleroom.ui.room.dao.IAnimeDao
import pe.com.dash.exampleroom.ui.room.database.AnimeDatabase

class AnimeRepository(application: Application) {
    private val animeDao: IAnimeDao?= AnimeDatabase.getInstance(application)?.getAnimeDao()

    fun insert(anime: Anime){
        if(animeDao != null) InsertAsyncTask(animeDao).execute(anime)
    }
    fun update(){

    }
    fun delete(){

    }
    fun findById(){

    }
    fun findAll(): LiveData<List<Anime>> {
        return animeDao?.findAll() ?: MutableLiveData<List<Anime>>()
    }




    private class InsertAsyncTask(private val animeDao: IAnimeDao): AsyncTask<Anime, Void, Void>(){
        override fun doInBackground(vararg animes: Anime?): Void? {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            for (anime in animes){
                if(anime!=null) animeDao.insert(anime)
            }
            return null
        }

    }
}