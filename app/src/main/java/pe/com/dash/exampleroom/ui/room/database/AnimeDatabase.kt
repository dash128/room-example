package pe.com.dash.exampleroom.ui.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import pe.com.dash.exampleroom.ui.room.dao.IAnimeDao
import pe.com.dash.exampleroom.ui.model.Anime

@Database(entities = arrayOf(Anime::class), version = 1)
abstract class AnimeDatabase:RoomDatabase() {
    abstract fun getAnimeDao(): IAnimeDao

    companion object{
        private var INSTANCE: AnimeDatabase?= null

        fun getInstance(context: Context): AnimeDatabase?{

            INSTANCE ?: synchronized(this){
                INSTANCE = Room.databaseBuilder(context, AnimeDatabase::class.java, "app").build()
            }

            return INSTANCE
        }
    }
}