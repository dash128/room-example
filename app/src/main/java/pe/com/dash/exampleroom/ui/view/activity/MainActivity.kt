package pe.com.dash.exampleroom.ui.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import pe.com.dash.exampleroom.R
import pe.com.dash.exampleroom.ui.adapter.AnimeAdapter
import pe.com.dash.exampleroom.ui.model.Anime
import pe.com.dash.exampleroom.ui.viewmodel.AnimeViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var animeViewModel: AnimeViewModel
    private lateinit var animeAdapter: AnimeAdapter
    private lateinit var animes: List<Anime>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadAnimes()
        animeAdapter = AnimeAdapter(animes)
        rvMainList.adapter = animeAdapter
        rvMainList.layoutManager = LinearLayoutManager(this)

        /*animeViewModel = run {
            return@run ViewModelProviders.of(this).get(AnimeViewModel::class.java)
        }

        //animeViewModel = run { ViewModelProviders.of(this).          //get(AnimeViewModel::class.java) }
        actionButtons()
        */
    }


    fun actionButtons(){
        btnMainAdd.setOnClickListener{
            startActivity(Intent(this, AnimeActivity::class.java))
        }
    }

    private fun addObserver(){
        val observer = Observer<List<Anime>>{
            animes->
                if (animes!=null){
                    var text = ""
                    for(anime in animes){

                    }
                }
        }
    }

    private fun loadAnimes(){
        animes = listOf(Anime(1,"Naruto","https://static.comicvine.com/uploads/original/11126/111264298/4940658-1035188845-01_Uz.jpg",2),
            Anime(2,"Jojos", "https://www.nautiljon.com/images/perso/00/04/trish_una_18240.jpg?0", 29)
        )
    }
}


