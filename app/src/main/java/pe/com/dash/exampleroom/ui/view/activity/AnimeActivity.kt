package pe.com.dash.exampleroom.ui.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_anime.*
import pe.com.dash.exampleroom.R

class AnimeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anime)


        actionButtons()

    }

    fun actionButtons(){
        btnAnimeOk.setOnClickListener{
            Toast.makeText(this,"Ok",Toast.LENGTH_SHORT).show()

        }
        btnAnimeBack.setOnClickListener{
            Toast.makeText(this,"Back",Toast.LENGTH_SHORT).show()
        }
    }
}
