package pe.com.dash.exampleroom.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.prototype_anime.view.*
import pe.com.dash.exampleroom.R
import pe.com.dash.exampleroom.ui.model.Anime

class AnimeAdapter(val animes: List<Anime>): RecyclerView.Adapter<AnimePrototype>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimePrototype {
        val myPrototype: AnimePrototype = AnimePrototype(
            LayoutInflater.from(parent.context).inflate(R.layout.prototype_anime, parent, false)
        )

        return myPrototype
    }

    override fun getItemCount(): Int { return animes.size }

    override fun onBindViewHolder(holder: AnimePrototype, position: Int) {
        holder.bind(animes[position])
    }

}

class AnimePrototype(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener{
    private var tvNameAnime = itemView.tvPrototypeAnime
    private var ivImagenAnime = itemView.ivPrototypeAnime

    init {
        itemView.setOnClickListener(this)
    }

    fun bind(anime: Anime){
        tvNameAnime.text = anime.name
        Picasso.get().load(anime.imagen).into(ivImagenAnime)
    }

    override fun onClick(v: View?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        Toast.makeText(v?.context,"Click"+position,Toast.LENGTH_SHORT).show()
    }
}
