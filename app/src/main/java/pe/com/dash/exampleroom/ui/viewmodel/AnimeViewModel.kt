package pe.com.dash.exampleroom.ui.viewmodel

import android.app.Application
import pe.com.dash.exampleroom.ui.model.Anime
import pe.com.dash.exampleroom.ui.room.repository.AnimeRepository

class AnimeViewModel(application: Application) {
    private val animeRepository = AnimeRepository(application)
    val animes = animeRepository.findAll()

    fun insert(anime: Anime){
        animeRepository.insert(anime)
    }
}